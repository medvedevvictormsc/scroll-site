gsap.registerPlugin(ScrollTrigger, ScrollSmoother);

ScrollSmoother.create({
  wrapper: ".wrapper",
  content: ".content",
  smooth: 1.5,
  effects: true,
});

gsap.fromTo(
  ".hero-section",
  { opacity: 1 },
  {
    opacity: 0,
    scrollTrigger: {
      trigger: ".hero-section",
      start: "center",
      end: "bottom",
      scrub: true,
    },
  }
);

let itemsL = gsap.utils.toArray(".gallery-left .gallery-item");
let itemsR = gsap.utils.toArray(".gallery-right .gallery-item");

itemsL.forEach((item) => {
  gsap.fromTo(
    item,
    { x: -500, opacity: 0 },
    {
      x: 0,
      opacity: 1,
      scrollTrigger: {
        trigger: item,
        end: "50",
        scrub: true,
      },
    }
  );
});

itemsR.forEach((item) => {
  gsap.fromTo(
    item,
    { x: 500, opacity: 0 },
    {
      x: 0,
      opacity: 1,
      scrollTrigger: {
        trigger: item,
        end: "60",
        scrub: true,
      },
    }
  );
});
